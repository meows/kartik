#   Alphabet

Alphabet means all the different kinds symbols you're allowed to write down. For example, the English alphabet has 26 symbols called letters:

`a b c d e f g h i j k l m n o p q r s t u v w x y z`

How about `λ`? Oops, that's not an English character. Or how about `アロホモラ`? Most people accept that these aren't English characters, so we don't build English words or sentences out of these.

In math, depending on what level you are at, your alphabet might only include the positive numbers: 

`0 1 2 3 4 5 6 7 8 9 + =`

Later on, you might learn about the negative numbers too, so your alphabet expands, and now you're allowed to work with problems and equations with symbols that look like:

`-9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 + - × ÷ =`

Eventually, your math alphabet may even expand to see such symbols like:

`π i ℝ ℤ Δ λ`

But if you're allowed to write it down, then it's an alphabet. At some point we have to agree on an alphabet in order to communicate easily.