#   Atom

An atom means a *sequence* of any symbols from your alphabet. In a sense they are like words. For example, let's say that our alphabet only allows the letters `a ... z`. Here are some example atoms using our rule:

  * `a` 
  * `b` 
  * `c` 
  * `dog` 
  * `eagle` 
  * `zzzfrancezzz`

Or, if we are allowed to use other symbols like `$ # @ $ ©`, we might create atoms like:

  * `#el@lo$`
  * `©@$#`

However, as a special rule, parenthesis like `( )` can't be used. That's because we use this as a special symbol to create lists.
