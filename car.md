#   `car` -- not the one you drive!

`car` is a special machine that eats lists and gives back the first thing in the
list.

  * The `car` of `(amy bob cat dog)`       is `amy`.
  * The `car` of `(0 1 2 3 4 5)`           is `0`.
  * The `car` of `((a b c) 1 2 3 (d e f))` is `(a b c)`.
  * The `car` of `(() 1 () 2 () 3)`        is `()`.
  * The `car` of `(3)`                     is `3`.

#   `cdr` -- the opposite of car

`cdr` is a machine that, unlike `car`, eats a list and gives back the same list
except with the first item removed.

  * The `cdr` of `(amy bob cat dog)`       is `(bob cat dog)`.
  * The `cdr` of `(0 1 2 3 4 5)`           is `(1 2 3 4 5)`.
  * The `cdr` of `((a b c) 1 2 3 (d e f))` is `(1 2 3 (d e f))`.
  * The `cdr` of `(() 1 () 2 () 3)`        is `(1 () 2 () 3)`.
  * The `cdr` of `(3)`                     is `()`.
