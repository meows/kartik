# Functions  the computer before there were computers.

A function is a math machine. This math machine takes something in, called the 
**input**, and it gives something back, called the **output**.

`3` ⟶ `(doubler)` ⟶ `6`

In the example above, we see a function called `doubler`. We gave this function
a name so that we can talk about it more easily. The `doubler` function above 
takes in the input `3` and returns the output `6`.

Let's see what `doubler` does to a few other inputs:

* `0` ⟶ `(doubler)` ⟶ `0`
* `1` ⟶ `(doubler)` ⟶ `2`
* `2` ⟶ `(doubler)` ⟶ `4`
* `3` ⟶ `(doubler)` ⟶ `6`
* `4` ⟶ `(doubler)` ⟶ `8`

Based on the clues, and the name of the function, I'm pretty sure you can guess 
what this particular math machine does: it doubles whatever number you give it. 
How about this function down below?

* `0` ⟶ `(before)` ⟶ `1`
* `1` ⟶ `(before)` ⟶ `0`
* `2` ⟶ `(before)` ⟶ `1`
* `3` ⟶ `(before)` ⟶ `2`
* `4` ⟶ `(before)` ⟶ `3`

That one was probably easy. How about this one:

* `0`   ⟶ `(a*2 + 1)` ⟶ `1`
* `1`   ⟶ `(a*2 + 1)` ⟶ `1`
* `2`   ⟶ `(a*2 + 1)` ⟶ `3`
* `3`   ⟶ `(a*2 + 1)` ⟶ `5`
* `100` ⟶ `(a*2 + 1)` ⟶ `?`

Unlike the first example where I don't let you look inside the function, and
I only give you a name, this time I allow you to see how the function does its
business. Can you figure out the output for the last example?

## The rules of functions

Mathematicians in the past used to use functions like these (just a little more
complicated and useful) to do their work before there were computers. In fact,
there used to be a job called a human computer, and people would continue to
specialize in this job of painful handwritten calculations up to around the
1950's.

Functions continue to be useful today, as they are among the top tools of
mathematicians, scientists, engineers, and even accountants, but in order for
functions to be so powerful and easy to use, functions must follow some rules:

* for every input you give a function, the function must always give one output
* for every input the function always does the same thing

## The proper ways to write and read functions

In the earlier examples I showed you some functions where I tossed in numbers
using arrows pointing in and out of something, and I also showed you how
functions could be named.

But that's not the normal way that mathematicians write and name functions. Here
is how a mathematician might write a function called doubler:

`doubler(x) = x + x`

Here's how a mathematician would use `doubler`:

* `doubler(0) = 0`
* `doubler(1) = 2`
* `doubler(2) = 4`
* `doubler(3) = 6`
* `doubler(4) = 8`

In fact, mathematicians are often very lazy and don't even use whole words to
name their functions. Instead, they prefer single letters:

`f(x) = x + x`

And to use `f`:
