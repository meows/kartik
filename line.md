f(x) -> 0

``` clojure
(require srfi/26)
(require plot)

(define (line x m b) (+ b (* m x)))

(define (compare m b)
   (let ([min -20] [max 20])
        (plot (list (axes)
                    (function (cut line <> 1 0) min max #:style 'dot #:color 'blue)
                    (function (cut line <> m b) min max))))
)

; explores the consequences of Δb
(define b (curry compare 1))

; explores the consequences of Δm
(define m (curryr compare 0))
```
