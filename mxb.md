# The boring kind of problem

1. A strawberry costs $2 each. Below is a function which tells you how much 
   money you have to pay for total strawberries:

* `cost(strawberries) = strawberries * 2`

2. Joe drives at 5 mph on average. Below is a function which tells you how many
   miles Joe will go depending on hours:

* `miles(hours) = 5 * hours`

Now you try:

3. Alice earns 20 euros per hour. Write a function which tells you how many euros
   Alice has earned based on hours.

* `euros(hours) =`

## The boring problem with a twist

* A phone call through AT&T costs 7¢ for the first minute, and 5¢ for every
  minute after that. The phone company rounds up to the nearest minute. Here's a
  function that tells you how many cents you owe per minutes called:

``` clojure
(define (cents minutes) 
   (+ 7 
      (* 5 minutes)
   )
)
```

* Alice just finished her phone call with her grandmother, and she received a
  bill of 122¢.

