##   Sequence
-------------

A sequence, *sometimes also called a list*, means a "line" of separate things, one after another, which you can count using the counting numbers (`0, 1, 2, 3, 4...`). So of course, the counting numbers form a sequence:

`0 -> 1 -> 2 -> ... -> 8 -> 9 -> 10`

People often recite the English alphabet as a sequence:

`a -> b -> c -> ... -> x -> y -> z`

Only anything you can count is a sequence! Some things in life don't naturally form a sequence, but yet if you can find a way to count them, then you can turn them into a sequence, such as people waiting in line, or even players on a basketball team:

### Golden State Warriors - Roster 2017

| sequence number | original | player 
| --------------- | -------- | ------
|  0              |  0       | Patrick McCaw
|  1              |  1       | JaVale McGee
|  2              |  2       | Jordan Bell
|  3              |  3       | David West
|  4              |  4       | Quinn Cook
|  5              |  5       | Kevon Looney
|  6              |  6       | Nick Young
|  7              | 11       | Klay Thompson
|  8              | 15       | Damian Jones
|  9              | 18       | Omri Casspi
| 10              | 23       | Draymond Green
| 11              | 25       | Chris Boucher
| 12              | 27       | Zaza Pachulia
| 13              | 30       | Stephen Curry
| 14              | 34       | Shaun Livingston
| 15              | 35       | Kevin Durant

Or even a list of words surrounded by parenthesis:

`(hello there i am surrounded by parenthesis)`

| list position | word
| ------------- | ----
| 0             | hello
| 1             | there 
| 2             | i
| 3             | am 
| 4             | surrounded  
| 5             | by 
| 6             | parenthesis
